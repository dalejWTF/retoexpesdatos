/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.Array.Ordenamiento;

import java.util.ArrayList;

/**
 *
 * @author SMART
 */
public class BurbujaArray {

    int[] array;

    public int[] transform(ArrayList<Integer> arr) {
        array = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            array[i] = arr.get(i);
        }
        burbuja(array);
        return array;
    }

    public int[] burbuja(int arr[]) {

        int i, j, temp;
        for (i = 0; i < arr.length; i++) {
            for (j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    // swap the elements
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }

        return arr;
    }
}
