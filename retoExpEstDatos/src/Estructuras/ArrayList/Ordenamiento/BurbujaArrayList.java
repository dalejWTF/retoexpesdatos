/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.ArrayList.Ordenamiento;

import java.util.ArrayList;

/**
 *
 * @author SMART
 */
public class BurbujaArrayList {

    public ArrayList<Integer> burbujaArrayList(ArrayList<Integer> arr) {

        int temp;
        for (int i = 0; i < arr.size(); i++) {
            for (int j = 0; j < arr.size() - i - 1; j++) {

                if (arr.get(j) > arr.get(j + 1)) {
                    temp = arr.get(j);
                    arr.set(j, arr.get(j + 1));
                    arr.set(j + 1, temp);

                }
            }
        }

        return arr;
    }
}
