/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.ArrayList.Ordenamiento;

import java.util.ArrayList;

/**
 *
 * @author kloch
 */
public class QuickSortArrayList {
    
     public ArrayList<Integer> quicksort(ArrayList<Integer> numeros, int izq, int der) {
        /*Este metodo recibe un arreglo de numero, y dos enteros que referencian el primer valor
            Y el ultimo*/
        //Se toma como pivote el primer valor
        int pivote = numeros.get(izq);
        //Se definen los dos lados y un auxiliar
        int i = izq;
        int j = der;
        int aux;
        while (i < j) {
            while (numeros.get(i) <= pivote && i < j) {
                i++;
            }

            while (numeros.get(j) > pivote) {
                j--;
            }
            if (i < j) {
                aux = numeros.get(i);
                numeros.set(i, numeros.get(j));
                numeros.set(j, aux);
            }
        }
        numeros.set(izq, numeros.get(j));
        numeros.set(j, pivote);
        if (izq < j - 1) {
            quicksort(numeros, izq, j - 1);
        }
        if (j + 1 < der) {
            quicksort(numeros, j + 1, der);
        }
        return numeros;
     }
    
}
