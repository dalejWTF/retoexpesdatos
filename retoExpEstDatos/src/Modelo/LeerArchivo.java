/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author SMART
 */
public class LeerArchivo {

    String path;
    public LeerArchivo(){}
    public LeerArchivo(String path) {
        this.path=path;
    }
    private Scanner entrada;

    public boolean abrirArchivo() {
        try {
            entrada = new Scanner(new File(path));
            return true;
        } // fin de try
        catch (FileNotFoundException fileNotFoundException) {
            JOptionPane.showMessageDialog(null, "Error al abrir el archivo", "Warning",
        JOptionPane.WARNING_MESSAGE);
            return false;
        } // fin de catch
    } // fin del m�todo abrirArchivo

    // lee registro del archivo
    public ArrayList<Integer> leerRegistros() {

        ArrayList<Integer> lista = new ArrayList<Integer>();
        try // lee registros del archivo, usando el objeto Scanner
        {

            while (entrada.hasNext()) {
                // System.out.println(entrada.nextLine());
                String cadena = entrada.nextLine();
                System.out.println(cadena);
                lista.add(Integer.valueOf(cadena));

            } // fin de while
        } // fin de try
        catch (NoSuchElementException elementException) {
            System.err.println("El archivo no esta bien formado.");
            entrada.close();
            System.exit(1);
        } // fin de catch
        catch (IllegalStateException stateException) {
            System.err.println("Error al leer del archivo.");
            System.exit(1);
        } // fin de catch
        return lista;
    }

    public void cerrarArchivo() {
        if (entrada != null) {
            entrada.close(); // cierra el archivo
        }
    }
}
