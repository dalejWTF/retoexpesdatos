/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.Stack.Busqueda;

import java.util.Iterator;
import java.util.Stack;


/**
 *
 * @author kloch
 */
public class StackSecuencial {
    
    public int Secuencial(Stack pila, int num){
         Boolean estado = false;
         Stack pilas = pila;
         for (Iterator iterator = pilas.iterator(); iterator.hasNext();) {
             Object next = iterator.next();
             int valor = Integer.parseInt((String) next);
             if(valor==num){
                 return num;
             }
         }
         return -1;
    }
    
}
