package Estructuras.Array.Busqueda;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kloch
 */
public class BusquedaBinariaArray {
    String message = "";
    public  int binarySearch(int[] arreglo, int dato) {
        int inicio = 0;
        int fin = arreglo.length-1;
        int pos;
        boolean encontrado;
        while (inicio <= fin) {
            pos = (inicio + fin) / 2;
            if (arreglo[pos] == dato) {
                return pos;
            } else if (arreglo[pos] < dato) {
                inicio = pos + 1;
            } else {
                fin = pos - 1;
            }
        }
        return -1;
    }
    public String convert(int [] arreglo, int dato){
        int valor = binarySearch(arreglo, dato);
         if (valor != -1) {
            message = "Sí hay algún valor " + dato + " en la lista.";
        } else {
            message = "No hay ningún valor " + dato + " en la lista.";
        }
        return message;
    } 

    
}
