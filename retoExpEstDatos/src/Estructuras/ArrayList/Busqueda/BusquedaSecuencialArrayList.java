/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.ArrayList.Busqueda;

import java.util.ArrayList;

/**
 *
 * @author kloch
 */
public class BusquedaSecuencialArrayList {
    
     int pos;

    public int secuencial(ArrayList<Integer> lista, int num) {	// Lista de números enteros que supondremos llena.
        boolean encontrado = false;	// Declaramos e inicializamos una bandera.
        
        for (int j = 0; j < lista.size(); j++) {
            if (lista.get(j)==num) {
                encontrado = true;
                pos=j;
            }
        }

        // Al finalizar el programa, la bandera nos indica si hemos encontrado el valor
        if (encontrado) {
            return pos;
        } else {
            return -1;
        }
    }
}
