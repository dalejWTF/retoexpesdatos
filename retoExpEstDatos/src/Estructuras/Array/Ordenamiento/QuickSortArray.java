/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.Array.Ordenamiento;

import java.util.ArrayList;

/**
 *
 * @author kloch
 */
public class QuickSortArray {
    int[] array;

    public int[] transform(ArrayList<Integer> arr, int izq, int der){
        array=new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            array[i]=arr.get(i);
        }
        quicksort(array,izq,der);
        return array;
    }
    
    public int[] quicksort(int [] numeros, int izq, int der) {
        /*Este metodo recibe un arreglo de numero, y dos enteros que referencian el primer valor
            Y el ultimo*/
        //Se toma como pivote el primer valor
        int pivote = numeros[izq];
        //Se definen los dos lados y un auxiliar
        int i = izq;
        int j = der;
        int aux;
        while (i < j) {
            while (numeros[i] <= pivote && i < j) {
                i++;
            }

            while (numeros[j] > pivote) {
                j--;
            }
            if (i < j) {
                aux = numeros[i];
                numeros[i]= numeros[j];
                numeros[j]= aux;
            }
        }
        numeros[izq]=numeros[j];
        numeros[j]= pivote;
        if (izq < j - 1) {
            quicksort(numeros, izq, j - 1);
        }
        if (j + 1 < der) {
            quicksort(numeros, j + 1, der);
        }
        return numeros;
     }
    
    
}
