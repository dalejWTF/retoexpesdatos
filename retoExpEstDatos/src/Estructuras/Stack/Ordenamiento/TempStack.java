/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.Stack.Ordenamiento;

import java.util.ArrayList;
import java.util.Stack;

/**
 *
 * @author SMART
 */
public class TempStack {
    Stack<Integer>input;
    public TempStack(ArrayList<Integer>data){
        for (Integer data1 : data) {
            input.add(data1);
        }
    }
    
    public  Stack<Integer> sortstack() 
    { 
        Stack<Integer> tmpStack = new Stack<Integer>(); 
        while(!input.isEmpty()) 
        { 
            // pop out the first element 
            int tmp = input.pop(); 
          
            // while temporary stack is not empty and 
            // top of stack is greater than temp 
            while(!tmpStack.isEmpty() && tmpStack.peek()  
                                                 > tmp) 
            { 
                // pop from temporary stack and  
                // push it to the input stack 
            input.push(tmpStack.pop()); 
            } 
              
            // push temp in tempory of stack 
            tmpStack.push(tmp); 
        } 
        return tmpStack; 
    } 
}
