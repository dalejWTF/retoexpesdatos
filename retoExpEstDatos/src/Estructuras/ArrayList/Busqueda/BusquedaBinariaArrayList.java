/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.ArrayList.Busqueda;

import java.util.ArrayList;

/**
 *
 * @author kloch
 */
public class BusquedaBinariaArrayList {
String message = "";
    public  int binarySearch(ArrayList<Integer> arreglo, int dato) {
        int inicio = 0;
        int fin = arreglo.size()-1;
        int pos;
        boolean encontrado;
        while (inicio <= fin) {
            pos = (inicio + fin) / 2;
            if (arreglo.get(pos) == dato) {
                return pos;
            } else if (arreglo.get(pos) < dato) {
                inicio = pos + 1;
            } else {
                fin = pos - 1;
            }
        }
        return -1;
    }

}
