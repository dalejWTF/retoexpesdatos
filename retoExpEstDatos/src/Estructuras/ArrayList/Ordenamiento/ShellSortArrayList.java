/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.ArrayList.Ordenamiento;

import java.util.ArrayList;

/**
 *
 * @author kloch
 */
public class ShellSortArrayList {
    public ArrayList<Integer> shell_sort(ArrayList<Integer> numeros) {
        int arr_len = numeros.size();
        for (int gap = arr_len / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < arr_len; i += 1) {
                int temp = numeros.get(i);
                int j;
                for (j = i; j >= gap && numeros.get(j - gap) > temp; j -= gap) {
                    numeros.set(j, numeros.get(j - gap));
                }
                numeros.set(j, temp);
            }
        }
        return numeros;
    }
   
}
