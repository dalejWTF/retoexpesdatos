/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.Array.Ordenamiento;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author SMART
 */
public class RadixSortArray {

    int[] array;

    public int[] transform(ArrayList<Integer> arr){
        array=new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            array[i]=arr.get(i);
        }
        radixSort(array);
        return array;
    }


    public static int[] radixSort(int[] input) {
        // Largest place for a 32-bit int is the 1 billion's place
        for (int place = 1; place <= 1000000000; place *= 10) {
            input = countingSort(input, place);
        }

        return input;
    }

    private static int[] countingSort(int[] input, int place) {
        int[] out = new int[input.length];

        int[] count = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            int digit = getDigit(input[i], place);
            count[digit] += 1;
        }

        for (int i = 1; i < count.length; i++) {
            count[i] += count[i - 1];
        }

        for (int i = input.length - 1; i >= 0; i--) {
            int digit = getDigit(input[i], place);

            out[count[digit] - 1] = input[i];
            count[digit]--;
        }

        return out;

    }

    private static int getDigit(int value, int digitPlace) {
        return ((value / digitPlace) % 10);
    }

    static void displayArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

}
