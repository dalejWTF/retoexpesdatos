/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.Queue.Ordenamiento;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author SMART
 */
public class RadixSortQUEUE {
        
    int[] array;

    public int[] transform(ArrayList<Integer> arr) {
        array = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            array[i] = arr.get(i);
        }
        radixSort(array);
        return array;
    }
        ///////////////////////////////////////////////////////////
        //                                                       //
        //                  RADIX SORT PARA Cola                 //
        //                                                       //
        //                                                       //
        //                                                       //
        ///////////////////////////////////////////////////////////
    
public static int[] radixSort(int[] input) {
		// Largest place for a 32-bit int is the 1 billion's place
		for (int place = 1; place <= 1000000000; place *= 10) {
			input = countingSort(input, place);
		}

		return input;
	}


	private static int[] countingSort(int[] input, int place) {
		int[] out = new int[input.length];
		Queue[] bucket = new Queue[input.length];

		for (int i = 0; i < 10; i++) {
			bucket[i] = new LinkedList<Integer>();
		}

		for (int i = 0; i < input.length; i++) {
			int digit = getDigit(input[i], place);
			bucket[digit].offer(input[i]);
		}

		int i = 0;
		int idx = 0;
		while (idx < input.length) {
			if (bucket[i].size() != 0) {
				out[idx++] = (int) bucket[i].poll();
			} else {
				i++;
			}
		}

		return out;

	}

	private static int getDigit(int value, int digitPlace) {
		return ((value / digitPlace) % 10);
	}

	static void displayArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
