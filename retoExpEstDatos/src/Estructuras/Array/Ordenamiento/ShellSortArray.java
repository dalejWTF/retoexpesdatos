/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras.Array.Ordenamiento;

import java.util.ArrayList;

/**
 *
 * @author kloch
 */
public class ShellSortArray {
    int[] array;

    public int[] transform(ArrayList<Integer> arr) {
        array = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            array[i] = arr.get(i);
        }
        shell_sort(array);
        return array;
    }
    
     public int[] shell_sort(int [] numeros) {
        int arr_len = numeros.length;
        for (int gap = arr_len / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < arr_len; i += 1) {
                int temp = numeros[i];
                int j;
                for (j = i; j >= gap && numeros[j - gap] > temp; j -= gap) {
                    numeros[j]= numeros[j - gap];
                }
                numeros[j]= temp;
            }
        }
        return numeros;
    }
    
     
     
}
